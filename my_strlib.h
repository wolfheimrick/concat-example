/*
**	Filename:	my_strlib.h
**	Author:		wolf
**	Email:		wolfheimrick@gir.ovh
**	Created:	2018-06-07 11:09:58
*/

#ifndef		__MY_STRLIB_H__
#define		__MY_STRLIB_H__

int my_strlen(const char *str);
void my_concat(char **destination, const char* str1, const char* str2);

#endif	/*	__MY_STRLIB_H__ */